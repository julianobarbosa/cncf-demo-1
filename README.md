### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.


* Webinar slide deck: https://docs.google.com/presentation/d/134891z7V3RpKAQMvrYorBSnywXxkGf8AMtuYtXLw2HQ/edit#slide=id.g4e37fd9035_1_368
* Message me on Twitter: https://twitter.com/thewilliamchia
* CNCF Trail Map: https://github.com/cncf/landscape/blob/master/README.md#trail-map 
* CNCF Top 30 High Velocity Projects: https://www.cncf.io/blog/2017/06/05/30-highest-velocity-open-source-projects/ 
* K8s single click install charts: https://docs.gitlab.com/ee/user/project/clusters/index.html#installing-applications
* GitLab, Runner, and Auto Deploy charts: https://gitlab.com/charts/charts.gitlab.io
* GitLab CI YAML config: https://docs.gitlab.com/ee/ci/yaml/
* Kubernetes Integration: https://about.gitlab.com/solutions/kubernetes/
* CNCF Multi-project, multi-cloud CI built on GitLab: https://cncf.ci/
* CNCF Case Study: https://about.gitlab.com/customers/cncf/
* https://about.gitlab.com/pricing/gitlab-com/feature-comparison/
* https://about.gitlab.com/direction/secure/
* Show MR with pipeline: https://gitlab.com/williamchia/cncf-demo-1/merge_requests/1
* Use yml file: https://gitlab.com/williamchia/cncf-demo-2
* Incremental rollout yml: https://gitlab.com/williamchia/cncf-demo-3/pipelines/44254161


